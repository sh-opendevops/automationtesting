const assert = require("assert");
const { Given, When, Then } = require("@cucumber/cucumber");

let app = require("../../src/app");

// Scenario: Purchase of two products # features/purchase_product.feature:7

When('the user purchase two products', function () {
  
  const products = app.catalogue.get('fridge')
  products.quantity = 2

  this.twoProducts = [products]
});

Then('the user does not get discount', function () {
  const response = app.totalPurchase(this.twoProducts)
  assert.deepEqual({'total': 4000, 'quantity': 2}, response);
});


// Scenario: Purchase of three products # features/purchase_product.feature:12

When('the user purchase three products', function () {
  
  const tv = app.catalogue.get('tv')
  tv.quantity = 2

  const cellphone = app.catalogue.get('cellphone')
  cellphone.quantity = 1

  this.threeProducts = [tv, cellphone]
});

Then('the user get discount of ten percent', function () {
  const response = app.totalPurchase(this.threeProducts)
  assert.deepEqual({'total': 4500, 'quantity': 3}, response);
});


// Scenario: Purchase of five products # features/purchase_product.feature:17

When('the user purchase five products', function () {
  
  const tv = app.catalogue.get('tv')
  tv.quantity = 1

  const laptop = app.catalogue.get('laptop')
  laptop.quantity = 2

  const cellphone = app.catalogue.get('cellphone')
  cellphone.quantity = 2

  this.threeProducts = [tv, laptop, cellphone]
});

Then('the user get discount of thirty percent', function () {
  const response = app.totalPurchase(this.threeProducts)
  assert.deepEqual({'total': 10500, 'quantity': 5}, response);
});
