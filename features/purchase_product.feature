@BDDSTORY-RTQ-22
@discount
Feature: Discount on purchase
  Discount on purchase of three or five products

  @BDDTEST-RTQ-23
  Scenario: purchase of two products
    When the user purchase two products
      Then the user does not get discount

  @BDDTEST-RTQ-24
  Scenario: Purchase of three products
    When the user purchase three products
    Then the user get discount of ten percent

  @BDDTEST-RTQ-25
  Scenario: Purchase of five products
    When the user purchase five products
    Then the user get discount of thirty percent