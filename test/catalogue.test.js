const { catalogue } = require('../src/app')

test('List of televisions in the catalog', () => {
  const result = catalogue.get('tv');
  expect({'brand': 'sony', 'price': 1000}).toMatchObject(result);
});

test('List of laptop in the catalog', () => {
  const result = catalogue.get('laptop');
  expect({'brand': 'dell', 'price': 4000}).toMatchObject(result);
});

test('List of fridge in the catalog', () => {
  const result = catalogue.get('fridge');
  expect({'brand': 'mabe', 'price': 2000}).toMatchObject(result);
});
