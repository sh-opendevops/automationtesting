const { discount } = require('../src/app')

test('A product', () => {
  const result = discount({'total': 1000, 'quantity': 1});
  expect({'total': 1000, 'quantity': 1}).toMatchObject(result);
});

test('Three products', () => {
  const result = discount({'total': 3000, 'quantity': 3});
  expect({'total': 2700, 'quantity': 3}).toMatchObject(result);
});

test('Five product', () => {
  const result = discount({'total': 10000, 'quantity': 5});
  expect({'total': 7000, 'quantity': 5}).toMatchObject(result);
});
