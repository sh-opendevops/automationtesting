const { totalPurchase } = require('../src/app')
const { catalogue } = require('../src/app');

test('A product', () => {
  const tv = catalogue.get('tv');
  tv.quantity = 1;
  
  const arrayProduct = [tv];
  const result = totalPurchase(arrayProduct);
  expect({'total': 1000, 'quantity': 1}).toMatchObject(result);
});

test('three product', () => {
  const fridge = catalogue.get('fridge');
  fridge.quantity = 3;

  const arrayProducts = [fridge];
  const result = totalPurchase(arrayProducts);
  expect({'total': 5400, 'quantity': 3}).toMatchObject(result);
});

test('three fridges and two cellphone', () => {
  const fridge = catalogue.get('laptop');
  fridge.quantity = 3;
  const cellphone = catalogue.get('cellphone');
  cellphone.quantity = 2;

  const arrayProducts = [fridge, cellphone];
  const result = totalPurchase(arrayProducts);
  expect({'total': 12600, 'quantity': 5}).toMatchObject(result);
});
