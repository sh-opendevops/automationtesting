const catalogue = new Map();

catalogue.set('tv', {'brand': 'sony', 'price': 1000})
catalogue.set('fridge', {'brand': 'mabe', 'price': 2000})
catalogue.set('cellphone', {'brand': 'samsung', 'price': 3000})
catalogue.set('laptop', {'brand': 'dell', 'price': 4000})

const discount = spending => {
  if(spending.quantity === 3) { spending.total = spending.total - spending.total * 0.10}
  if(spending.quantity === 5) { spending.total = spending.total - spending.total * 0.30}

  return {'total': spending.total, 'quantity': spending.quantity}
}

// Array of products
// sum of products and total 
const totalPurchase = array => {
  if(array === undefined || array.length === 0) return 0

  let total = 0
  let quantity = 0
  array.forEach(product => {
    total += product.price * product.quantity
    quantity += product.quantity 
  })

  return discount({'total': total, 'quantity': quantity})
 
}


module.exports = {
  catalogue,
  discount,
  totalPurchase  
}